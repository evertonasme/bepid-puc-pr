//
//  Personagem.h
//  RPG
//
//  Created by Mark Joselli on 3/4/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Personagem : NSObject



@property NSString* nome;
@property NSString* tipo;
@property NSString *descricaoFileName;
@property NSString *imgFileName;
@property NSMutableDictionary *equipamento;

-(instancetype)initWithNome:(NSString*)nome;
-(instancetype)initWithNome:(NSString*)nome Tipo:(NSString*)tipo DescricaoFileName:(NSString*)descricaoFileName ImageFileName:(NSString*) imgFilename;

-(instancetype)initWithNome:(NSString*)nome Tipo:(NSString*)tipo DescricaoFileName:(NSString*)descricaoFileName ImageFileName:(NSString*) imgFilename Armadura:(NSString*)armadura Arma:(NSString*) arma;

-(void) AddEquipment:(NSString*)tipo withPoderes:(NSString*)descricao;

-(NSString*) GetDescricao;

@end

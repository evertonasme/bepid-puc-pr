//
//  SelectionHeroViewController.h
//  RPG
//
//  Created by Mark Joselli on 3/5/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "ViewController.h"

@interface SelectionHeroViewController : ViewController{
    NSArray *meusHerois;
    int currentSelected;
    
}
@property (weak, nonatomic) IBOutlet UILabel *NomeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *FotoImage;
@property (weak, nonatomic) IBOutlet UILabel *ClasseLabel;
@property (weak, nonatomic) IBOutlet UITextView *DescricaoText;
@property (weak, nonatomic) IBOutlet UIButton *LeftButton;
@property (weak, nonatomic) IBOutlet UIButton *RightButton;

- (IBAction)LeftPressed:(id)sender;
- (IBAction)RightPressed:(id)sender;

@end

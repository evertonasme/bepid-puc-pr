//
//  SelectionHeroViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/5/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "SelectionHeroViewController.h"

@interface SelectionHeroViewController ()

@end

@implementation SelectionHeroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Popular];
    
    currentSelected = 0;
    
    [self LoadSelectedHero];
    
    
    [self ChangeToHeroWithIndex:currentSelected];
    
}

-(void)ChangeToHeroWithIndex:(int)index{
    if(index < 0 || index >= meusHerois.count){
        NSLog(@"Erro personagem com indice %d não existe",index);
        return;
        
    }
    
    Personagem *current = [meusHerois objectAtIndex:index];
    
    _NomeLabel.text = [NSString stringWithFormat:@"Nome: %@",current.nome] ;
    _FotoImage.image = [UIImage imageNamed:current.imgFileName];
    _ClasseLabel.text = [NSString stringWithFormat:@"Classe: %@",[current.tipo uppercaseString]] ;
    _DescricaoText.text = [current GetDescricao];
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Popular{
    meusHerois = [[NSArray alloc]initWithObjects:
                  [[Personagem alloc]initWithNome:@"Binder" Tipo:@"Chefe" DescricaoFileName:@"binder.txt" ImageFileName:@"binder.png" Armadura:@"Manto Mágico" Arma:nil],
                  [[Personagem alloc]initWithNome:@"Micheli" Tipo:@"Ranger" DescricaoFileName:@"micheli.txt" ImageFileName:@"micheli.png" Armadura:@"Colete Mágico" Arma:@"Arco de Fogo"],
                  [[Personagem alloc]initWithNome:@"Breno" Tipo:@"Mago" DescricaoFileName:@"breno.txt" ImageFileName:@"breno.png" Armadura:@"Manto Verde" Arma:@"Gorro Mágico"],
                  [[Personagem alloc]initWithNome:@"Maicris" Tipo:@"Druida" DescricaoFileName:@"maicris.txt" ImageFileName:@"maicris.png" Armadura:@"Colete Simples" Arma:@"Coração"],
                  [[Personagem alloc]initWithNome:@"Mark" Tipo:@"Guerreiro" DescricaoFileName:@"mark.txt" ImageFileName:@"mark.png" Armadura:@"Suspensório" Arma:@"Porrete"],
                  nil];
    

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)LeftPressed:(id)sender {
    if(--currentSelected <= 0){
        currentSelected = 0;
        _LeftButton.hidden = YES;
    }
     _RightButton.hidden = NO;
    [self SaveSelectedHero];
    [self ChangeToHeroWithIndex:currentSelected];
}

- (IBAction)RightPressed:(id)sender {
    if(++currentSelected > meusHerois.count-2){
        currentSelected = (int)meusHerois.count-1;
        _RightButton.hidden = YES;
    }
    _LeftButton.hidden = NO;
    [self SaveSelectedHero];
    [self ChangeToHeroWithIndex:currentSelected];
}

-(void)LoadSelectedHero{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    
    
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"currentSelected.txt"];
    
    NSError *error;

    NSString *currentSelectedString = [[NSString alloc] initWithContentsOfFile:documentsDirectory encoding:NSUTF8StringEncoding error:&error];
    
    if (currentSelectedString == nil){
        // Handle error here
        NSLog(@"Erro:%@",[error description]);
    }else{
        currentSelected = [currentSelectedString intValue];
        
    }
}

-(void)SaveSelectedHero{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    NSLog(@"%@",documentsDirectory);
    
    
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"currentSelected.txt"];
    
    NSError *error;
    NSString *myString = [NSString stringWithFormat:@"%d",currentSelected];
    BOOL succeed = [myString writeToFile:documentsDirectory
                              atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (!succeed){
        // Handle error here
        NSLog(@"Erro:%@",[error description]);
    }
}
@end

//
//  AppDelegate.h
//  RPG
//
//  Created by Mark Joselli on 3/4/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


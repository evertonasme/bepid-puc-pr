//
//  Personagem.h
//  RPG
//
//  Created by Mark Joselli on 3/4/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Personagem : NSObject{
    NSString *teste;
}



@property NSString* nome;
@property NSString* tipo;
@property NSString *descricao;
@property NSString *imgPath;

-(instancetype)initWithNome:(NSString*)nome;
-(instancetype)initWithNome:(NSString*)nome Tipo:(NSString*)tipo Descricao:(NSString*)descricao ImagePath:(NSString*) imgPath;

@end
